const createResult = require('./resultModel');

describe('result model test', () => {
  it('empty result is empty', () => {
    const arr = [];
    const resultArray = createResult(arr);
    expect(resultArray.isEmpty()).toBe(true);
  });
  it('non empty result is not empty', () => {
    const arr = [1, 2, 3];
    const resultArray = createResult(arr);
    expect(resultArray.isEmpty()).toBe(false);
  });
  it('returned array of books', () => {
    const arr = [1, 2, 3];
    const resultArray = createResult(arr).getResultArray();
    expect(resultArray).toStrictEqual(arr);
  });
  it('filters with a name filter', () => {
    const filteredArray = [
      { name: 'a', author: 'w' },
      { name: 'b', author: 'w' },
      { name: 'd', author: 'w' },
    ];
    const filter = { name: 'a' };
    const resultArray = createResult(filteredArray).findMany(filter);
    expect(resultArray).toStrictEqual([{ name: 'a', author: 'w' }]);
  });
  it('filters with a author filter', () => {
    const filteredArray = [
      { name: 'a', author: 'w' },
      { name: 'b', author: 'ws' },
      { name: 'd', author: 'w' },
    ];
    const filter = { author: 'w' };
    const resultArray = createResult(filteredArray).findMany(filter);
    expect(resultArray).toStrictEqual([
      { name: 'a', author: 'w' },
      { name: 'd', author: 'w' },
    ]);
  });
  it('disregards author if name is given', () => {
    const filteredArray = [
      { name: 'a', author: 'w' },
      { name: 'b', author: 'ws' },
      { name: 'd', author: 'w' },
    ];
    const filter = { name: 'a', author: 'w' };
    const resultArray = createResult(filteredArray).findMany(filter);
    expect(resultArray).toStrictEqual([{ name: 'a', author: 'w' }]);
  });
  it.skip('filters with two name filters', () => {
    const filteredArray = [
      { name: 'a', author: 'w' },
      { name: 'b', author: 'w' },
      { name: 'd', author: 'w' },
    ];
    const filter = { name: ['a', 'b'] };
    const resultArray = createResult(filteredArray).findMany(filter);
    expect(resultArray).toStrictEqual([
      { name: 'a', author: 'w' },
      { name: 'b', author: 'w' },
    ]);
  });
});
