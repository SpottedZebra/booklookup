/* eslint-disable no-alert */
const fetch = require('node-fetch');

const OPEN_LIB_ENDPOINT = 'https://openlibrary.org/search.json';

const openLibFacade = () => {
  return {
    async getBookByISBN(isbn) {
      const response = await fetch(`${OPEN_LIB_ENDPOINT}?q=${isbn}`);
      if (response.ok) {
        const json = await response.json();
        const result = {
          publishedYear: json.docs[0].first_publish_year,
          author: json.docs[0].author_name[0],
          name: json.docs[0].title_suggest,
          ISBN: isbn,
        };
        return result;
      }
      alert('There is an error');
      return false;
    },
  };
};

module.exports = openLibFacade;
