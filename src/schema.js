const { gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    author: String!
    name: String!
    ISBN: String!
    publishedYear: String
  }

  input BookInput {
    author: [String]
    name: String
  }

  input NewBookInput {
    author: String!
    name: String!
    ISBN: String!
  }

  input LookupBookISBNInput {
    ISBN: String!
  }

  # not used until i find a way to unionise it with isbn lookup
  input LookupBookNameInput {
    name: String!
  }

  input RemoveBookInput {
    name: String!
  }

  type Query {
    book(input: BookInput): [Book]!
    lookupBook(input: LookupBookISBNInput!): Book!
  }

  type Mutation {
    addBook(input: NewBookInput!): Book!
    removeBook(input: RemoveBookInput!): Boolean!
  }
`;

module.exports = typeDefs;
