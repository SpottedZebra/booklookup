const createResultModel = (resultsArray) => {
  return {
    isEmpty() {
      return resultsArray.length === 0;
    },
    getResultArray() {
      return resultsArray;
    },
    findMany(filter) {
      return resultsArray.filter((result) => {
        if (filter.name) return filter.name === result.name;
        if (filter.author) return filter.author === result.author;
      });
    },
  };
};

module.exports = createResultModel;
