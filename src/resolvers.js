const actions = require('../db/actions');
const createResultModel = require('./resultModel');
const openLibFacade = require('./openLibFacade');

module.exports = {
  Query: {
    async book(context, { input }) {
      const results = await actions.getAllBooks();
      if (results) {
        const booksArray = [];
        Object.keys(results).forEach((key) => {
          booksArray.push({
            name: results[key].name,
            author: results[key].author,
            ISBN: results[key].isbn,
          });
        });
        const resultsModel = createResultModel(booksArray);
        if (input) {
          return resultsModel.findMany(input);
        }
        return resultsModel.getResultArray();
      }
      return [];
    },
    async lookupBook(context, { input }) {
      const result = await openLibFacade().getBookByISBN(input.ISBN);
      return result;
    },
  },
  Mutation: {
    async addBook(context, { input }) {
      const result = await actions.addBook(input.name, input.author, input.ISBN);
      return result;
    },
    async removeBook(context, { input }) {
      const result = await actions.removeBook(input.name);
      return result;
    },
  },
};
