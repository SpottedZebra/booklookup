const sqlite3 = require('sqlite3');
const path = require('path');
const fs = require('fs');
const util = require('util');

const DB_PATH = path.join(__dirname, 'my.db');
const DB_SQL_PATH = path.join(__dirname, 'init.sql');

const myDB = new sqlite3.Database(DB_PATH);
const SQL3 = {
  run(...args) {
    return new Promise((resolve, reject) => {
      myDB.run(...args, function onResult(err) {
        if (err) reject(err);
        else resolve(this);
      });
    });
  },
  get: util.promisify(myDB.get.bind(myDB)),
  all: util.promisify(myDB.all.bind(myDB)),
  exec: util.promisify(myDB.exec.bind(myDB)),
};

async function main() {
  const initSQL = fs.readFileSync(DB_SQL_PATH, 'utf-8');
  await SQL3.exec(initSQL);
}

main();

// const { Client } = require('pg');

// const client = new Client({
//   connectionString: process.env.DATABASE_URL,
//   ssl: false,
// });

// client.connect();
