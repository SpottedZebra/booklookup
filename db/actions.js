const sqlite3 = require('sqlite3');
const path = require('path');
const util = require('util');

const DB_PATH = path.join(__dirname, 'my.db');

const myDB = new sqlite3.Database(DB_PATH);
const SQL3 = {
  run(...args) {
    return new Promise((resolve, reject) => {
      myDB.run(...args, function onResult(err) {
        if (err) reject(err);
        else resolve(this);
      });
    });
  },
  get: util.promisify(myDB.get.bind(myDB)),
  all: util.promisify(myDB.all.bind(myDB)),
  exec: util.promisify(myDB.exec.bind(myDB)),
};

async function getAllBooksFromTable() {
  const result = await SQL3.all(
    `
      SELECT
        name, author, isbn
      FROM
        Books
    `,
  );

  if (result && result.length > 0) {
    return result;
  }
  return false;
}

async function addBookToTable(name, author, isbn) {
  const result = await SQL3.all(
    `
      INSERT INTO 
        Books
        (name, author, isbn)
      VALUES
        (?, ?, ?)
    `,
    name,
    author,
    isbn,
  ).catch((error) => console.warn(error));
  if (result && result.length > 0) {
    return result;
  }
  return false;
}

async function deleteBookFromTable(name) {
  const result = await SQL3.all(
    `
       DELETE FROM
        Books
      WHERE
        name = ?
    `,
    name,
  ).catch((error) => console.warn(error));
  console.log(result);
  if (result && result.length > 0) {
    return result;
  }

  return false;
}

const getAllBooks = async () => getAllBooksFromTable();
const addBook = async (name, author, isbn) => addBookToTable(name, author, isbn);
const removeBook = async (name) => deleteBookFromTable(name);

module.exports = {
  getAllBooks,
  addBook,
  removeBook,
};
